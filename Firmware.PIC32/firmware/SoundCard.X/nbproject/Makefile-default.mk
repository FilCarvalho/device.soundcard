#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/SoundCard.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/SoundCard.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/app.c ../src/main.c ../src/sounds_allocation.c ../src/memory.c ../src/audio.c ../src/ios.c ../src/delay.c ../src/parallel_bus.c ../../../../../../../../../microchip/harmony/v2_04/framework/driver/i2s/src/dynamic/drv_i2s_dma.c ../../../../../../../../../microchip/harmony/v2_04/framework/driver/tmr/src/dynamic/drv_tmr.c ../../../../../../../../../microchip/harmony/v2_04/framework/system/dma/src/sys_dma.c ../../../../../../../../../microchip/harmony/v2_04/framework/system/int/src/sys_int_pic32.c ../../../../../../../../../microchip/harmony/v2_04/framework/system/tmr/src/sys_tmr.c ../../../../../../../../../microchip/harmony/v2_04/framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c ../../../../../../../../../microchip/harmony/v2_04/framework/usb/src/dynamic/usb_device.c ../../../../../../../../../microchip/harmony/v2_04/framework/driver/usb/usbhs/src/dynamic/drv_usbhs_device.c ../../../../../../../../../microchip/harmony/v2_04/framework/usb/src/dynamic/usb_device_endpoint_functions.c ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c ../src/system_config/default/framework/system/devcon/src/sys_devcon.c ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S ../src/system_config/default/framework/system/ports/src/sys_ports_static.c ../src/system_config/default/framework/system/reset/src/sys_reset.c ../src/system_config/default/system_init.c ../src/system_config/default/system_interrupt.c ../src/system_config/default/system_exceptions.c ../src/system_config/default/system_tasks.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1360937237/app.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1360937237/sounds_allocation.o ${OBJECTDIR}/_ext/1360937237/memory.o ${OBJECTDIR}/_ext/1360937237/audio.o ${OBJECTDIR}/_ext/1360937237/ios.o ${OBJECTDIR}/_ext/1360937237/delay.o ${OBJECTDIR}/_ext/1360937237/parallel_bus.o ${OBJECTDIR}/_ext/1450069464/drv_i2s_dma.o ${OBJECTDIR}/_ext/1383485383/drv_tmr.o ${OBJECTDIR}/_ext/176760579/sys_dma.o ${OBJECTDIR}/_ext/11966580/sys_int_pic32.o ${OBJECTDIR}/_ext/1154096286/sys_tmr.o ${OBJECTDIR}/_ext/134701902/drv_usbhs.o ${OBJECTDIR}/_ext/249061497/usb_device.o ${OBJECTDIR}/_ext/134701902/drv_usbhs_device.o ${OBJECTDIR}/_ext/249061497/usb_device_endpoint_functions.o ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o ${OBJECTDIR}/_ext/340578644/sys_devcon.o ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o ${OBJECTDIR}/_ext/822048611/sys_ports_static.o ${OBJECTDIR}/_ext/68765530/sys_reset.o ${OBJECTDIR}/_ext/1688732426/system_init.o ${OBJECTDIR}/_ext/1688732426/system_interrupt.o ${OBJECTDIR}/_ext/1688732426/system_exceptions.o ${OBJECTDIR}/_ext/1688732426/system_tasks.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1360937237/app.o.d ${OBJECTDIR}/_ext/1360937237/main.o.d ${OBJECTDIR}/_ext/1360937237/sounds_allocation.o.d ${OBJECTDIR}/_ext/1360937237/memory.o.d ${OBJECTDIR}/_ext/1360937237/audio.o.d ${OBJECTDIR}/_ext/1360937237/ios.o.d ${OBJECTDIR}/_ext/1360937237/delay.o.d ${OBJECTDIR}/_ext/1360937237/parallel_bus.o.d ${OBJECTDIR}/_ext/1450069464/drv_i2s_dma.o.d ${OBJECTDIR}/_ext/1383485383/drv_tmr.o.d ${OBJECTDIR}/_ext/176760579/sys_dma.o.d ${OBJECTDIR}/_ext/11966580/sys_int_pic32.o.d ${OBJECTDIR}/_ext/1154096286/sys_tmr.o.d ${OBJECTDIR}/_ext/134701902/drv_usbhs.o.d ${OBJECTDIR}/_ext/249061497/usb_device.o.d ${OBJECTDIR}/_ext/134701902/drv_usbhs_device.o.d ${OBJECTDIR}/_ext/249061497/usb_device_endpoint_functions.o.d ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d ${OBJECTDIR}/_ext/340578644/sys_devcon.o.d ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d ${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d ${OBJECTDIR}/_ext/68765530/sys_reset.o.d ${OBJECTDIR}/_ext/1688732426/system_init.o.d ${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d ${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d ${OBJECTDIR}/_ext/1688732426/system_tasks.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1360937237/app.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1360937237/sounds_allocation.o ${OBJECTDIR}/_ext/1360937237/memory.o ${OBJECTDIR}/_ext/1360937237/audio.o ${OBJECTDIR}/_ext/1360937237/ios.o ${OBJECTDIR}/_ext/1360937237/delay.o ${OBJECTDIR}/_ext/1360937237/parallel_bus.o ${OBJECTDIR}/_ext/1450069464/drv_i2s_dma.o ${OBJECTDIR}/_ext/1383485383/drv_tmr.o ${OBJECTDIR}/_ext/176760579/sys_dma.o ${OBJECTDIR}/_ext/11966580/sys_int_pic32.o ${OBJECTDIR}/_ext/1154096286/sys_tmr.o ${OBJECTDIR}/_ext/134701902/drv_usbhs.o ${OBJECTDIR}/_ext/249061497/usb_device.o ${OBJECTDIR}/_ext/134701902/drv_usbhs_device.o ${OBJECTDIR}/_ext/249061497/usb_device_endpoint_functions.o ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o ${OBJECTDIR}/_ext/340578644/sys_devcon.o ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o ${OBJECTDIR}/_ext/822048611/sys_ports_static.o ${OBJECTDIR}/_ext/68765530/sys_reset.o ${OBJECTDIR}/_ext/1688732426/system_init.o ${OBJECTDIR}/_ext/1688732426/system_interrupt.o ${OBJECTDIR}/_ext/1688732426/system_exceptions.o ${OBJECTDIR}/_ext/1688732426/system_tasks.o

# Source Files
SOURCEFILES=../src/app.c ../src/main.c ../src/sounds_allocation.c ../src/memory.c ../src/audio.c ../src/ios.c ../src/delay.c ../src/parallel_bus.c ../../../../../../../../../microchip/harmony/v2_04/framework/driver/i2s/src/dynamic/drv_i2s_dma.c ../../../../../../../../../microchip/harmony/v2_04/framework/driver/tmr/src/dynamic/drv_tmr.c ../../../../../../../../../microchip/harmony/v2_04/framework/system/dma/src/sys_dma.c ../../../../../../../../../microchip/harmony/v2_04/framework/system/int/src/sys_int_pic32.c ../../../../../../../../../microchip/harmony/v2_04/framework/system/tmr/src/sys_tmr.c ../../../../../../../../../microchip/harmony/v2_04/framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c ../../../../../../../../../microchip/harmony/v2_04/framework/usb/src/dynamic/usb_device.c ../../../../../../../../../microchip/harmony/v2_04/framework/driver/usb/usbhs/src/dynamic/drv_usbhs_device.c ../../../../../../../../../microchip/harmony/v2_04/framework/usb/src/dynamic/usb_device_endpoint_functions.c ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c ../src/system_config/default/framework/system/devcon/src/sys_devcon.c ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S ../src/system_config/default/framework/system/ports/src/sys_ports_static.c ../src/system_config/default/framework/system/reset/src/sys_reset.c ../src/system_config/default/system_init.c ../src/system_config/default/system_interrupt.c ../src/system_config/default/system_exceptions.c ../src/system_config/default/system_tasks.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/SoundCard.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MZ2048EFM100
MP_LINKER_FILE_OPTION=,--script="..\src\system_config\default\app_mz.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  .generated_files/flags/default/3e9ae28729c36e2666e7a99018fd99bef860c082 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.ok ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.err 
	${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1 -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d"  -o ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_ICD4=1 
	@${FIXDEPS} "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d" "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  .generated_files/flags/default/6fbf2573e91a8c4ff5f77895a76f5a3151bce852 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.ok ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.err 
	${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d"  -o ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.asm.d",--gdwarf-2 
	@${FIXDEPS} "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d" "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1360937237/app.o: ../src/app.c  .generated_files/flags/default/3d3721204e96c7ee1b3402c497702d16756a9d9 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/app.o.d" -o ${OBJECTDIR}/_ext/1360937237/app.o ../src/app.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  .generated_files/flags/default/f8b03819a97bfb240bb1ea28f09fd51952392084 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/sounds_allocation.o: ../src/sounds_allocation.c  .generated_files/flags/default/eb852508ed61c199826c9fd5e5dcb4de820a11e1 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/sounds_allocation.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/sounds_allocation.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/sounds_allocation.o.d" -o ${OBJECTDIR}/_ext/1360937237/sounds_allocation.o ../src/sounds_allocation.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/memory.o: ../src/memory.c  .generated_files/flags/default/10f0a1e10dacfdb953941bc5cd50e62d7bf30d39 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/memory.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/memory.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/memory.o.d" -o ${OBJECTDIR}/_ext/1360937237/memory.o ../src/memory.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/audio.o: ../src/audio.c  .generated_files/flags/default/5fb618af924933d70294958c3321911848426553 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/audio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/audio.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/audio.o.d" -o ${OBJECTDIR}/_ext/1360937237/audio.o ../src/audio.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/ios.o: ../src/ios.c  .generated_files/flags/default/44609fcdcc85d6074ae82ac093e7b2d7b34f2db7 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ios.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ios.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ios.o.d" -o ${OBJECTDIR}/_ext/1360937237/ios.o ../src/ios.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/delay.o: ../src/delay.c  .generated_files/flags/default/b295d61be7b2f1eac7694883242afef01ea45dd2 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/delay.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/delay.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/delay.o.d" -o ${OBJECTDIR}/_ext/1360937237/delay.o ../src/delay.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/parallel_bus.o: ../src/parallel_bus.c  .generated_files/flags/default/d8eb3c615035101d6f652215cd8d506b3e1f9703 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/parallel_bus.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/parallel_bus.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/parallel_bus.o.d" -o ${OBJECTDIR}/_ext/1360937237/parallel_bus.o ../src/parallel_bus.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1450069464/drv_i2s_dma.o: ../../../../../../../../../microchip/harmony/v2_04/framework/driver/i2s/src/dynamic/drv_i2s_dma.c  .generated_files/flags/default/d964569c03f0b562a72c29c0fb51fb1ddc2a9353 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1450069464" 
	@${RM} ${OBJECTDIR}/_ext/1450069464/drv_i2s_dma.o.d 
	@${RM} ${OBJECTDIR}/_ext/1450069464/drv_i2s_dma.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1450069464/drv_i2s_dma.o.d" -o ${OBJECTDIR}/_ext/1450069464/drv_i2s_dma.o ../../../../../../../../../microchip/harmony/v2_04/framework/driver/i2s/src/dynamic/drv_i2s_dma.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1383485383/drv_tmr.o: ../../../../../../../../../microchip/harmony/v2_04/framework/driver/tmr/src/dynamic/drv_tmr.c  .generated_files/flags/default/65456e14bfd05c47aab7406c6529d20ca4a1d165 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1383485383" 
	@${RM} ${OBJECTDIR}/_ext/1383485383/drv_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/1383485383/drv_tmr.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1383485383/drv_tmr.o.d" -o ${OBJECTDIR}/_ext/1383485383/drv_tmr.o ../../../../../../../../../microchip/harmony/v2_04/framework/driver/tmr/src/dynamic/drv_tmr.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/176760579/sys_dma.o: ../../../../../../../../../microchip/harmony/v2_04/framework/system/dma/src/sys_dma.c  .generated_files/flags/default/1e6924e456d6826d0e800615633625533dc92c0e .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/176760579" 
	@${RM} ${OBJECTDIR}/_ext/176760579/sys_dma.o.d 
	@${RM} ${OBJECTDIR}/_ext/176760579/sys_dma.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/176760579/sys_dma.o.d" -o ${OBJECTDIR}/_ext/176760579/sys_dma.o ../../../../../../../../../microchip/harmony/v2_04/framework/system/dma/src/sys_dma.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/11966580/sys_int_pic32.o: ../../../../../../../../../microchip/harmony/v2_04/framework/system/int/src/sys_int_pic32.c  .generated_files/flags/default/b45e6b4a4cbd800f6aadae29e318d62a8597ccd6 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/11966580" 
	@${RM} ${OBJECTDIR}/_ext/11966580/sys_int_pic32.o.d 
	@${RM} ${OBJECTDIR}/_ext/11966580/sys_int_pic32.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/11966580/sys_int_pic32.o.d" -o ${OBJECTDIR}/_ext/11966580/sys_int_pic32.o ../../../../../../../../../microchip/harmony/v2_04/framework/system/int/src/sys_int_pic32.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1154096286/sys_tmr.o: ../../../../../../../../../microchip/harmony/v2_04/framework/system/tmr/src/sys_tmr.c  .generated_files/flags/default/7666090e6d767400c5f87dcdece366267ae6f381 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1154096286" 
	@${RM} ${OBJECTDIR}/_ext/1154096286/sys_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/1154096286/sys_tmr.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1154096286/sys_tmr.o.d" -o ${OBJECTDIR}/_ext/1154096286/sys_tmr.o ../../../../../../../../../microchip/harmony/v2_04/framework/system/tmr/src/sys_tmr.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/134701902/drv_usbhs.o: ../../../../../../../../../microchip/harmony/v2_04/framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c  .generated_files/flags/default/bfb1d543aef21d86cc3cb57a0a56b3e4bff2a895 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/134701902" 
	@${RM} ${OBJECTDIR}/_ext/134701902/drv_usbhs.o.d 
	@${RM} ${OBJECTDIR}/_ext/134701902/drv_usbhs.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/134701902/drv_usbhs.o.d" -o ${OBJECTDIR}/_ext/134701902/drv_usbhs.o ../../../../../../../../../microchip/harmony/v2_04/framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/249061497/usb_device.o: ../../../../../../../../../microchip/harmony/v2_04/framework/usb/src/dynamic/usb_device.c  .generated_files/flags/default/eaa889a9b6ddfc466543a3950661a5809d2d9154 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/249061497" 
	@${RM} ${OBJECTDIR}/_ext/249061497/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/249061497/usb_device.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/249061497/usb_device.o.d" -o ${OBJECTDIR}/_ext/249061497/usb_device.o ../../../../../../../../../microchip/harmony/v2_04/framework/usb/src/dynamic/usb_device.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/134701902/drv_usbhs_device.o: ../../../../../../../../../microchip/harmony/v2_04/framework/driver/usb/usbhs/src/dynamic/drv_usbhs_device.c  .generated_files/flags/default/505caddf9a4581878ad9b46e200c1b4f0098756b .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/134701902" 
	@${RM} ${OBJECTDIR}/_ext/134701902/drv_usbhs_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/134701902/drv_usbhs_device.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/134701902/drv_usbhs_device.o.d" -o ${OBJECTDIR}/_ext/134701902/drv_usbhs_device.o ../../../../../../../../../microchip/harmony/v2_04/framework/driver/usb/usbhs/src/dynamic/drv_usbhs_device.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/249061497/usb_device_endpoint_functions.o: ../../../../../../../../../microchip/harmony/v2_04/framework/usb/src/dynamic/usb_device_endpoint_functions.c  .generated_files/flags/default/c63396349cb9a6c312b0b27074a54007ec3b136e .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/249061497" 
	@${RM} ${OBJECTDIR}/_ext/249061497/usb_device_endpoint_functions.o.d 
	@${RM} ${OBJECTDIR}/_ext/249061497/usb_device_endpoint_functions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/249061497/usb_device_endpoint_functions.o.d" -o ${OBJECTDIR}/_ext/249061497/usb_device_endpoint_functions.o ../../../../../../../../../microchip/harmony/v2_04/framework/usb/src/dynamic/usb_device_endpoint_functions.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o: ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c  .generated_files/flags/default/107cb373666ab4fc34040ed568492dd58e2eaef4 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/639803181" 
	@${RM} ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d" -o ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/340578644/sys_devcon.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon.c  .generated_files/flags/default/dd2cbf70751e2b89f30963ebd8b522cd2ac2e120 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon.o.d" -o ${OBJECTDIR}/_ext/340578644/sys_devcon.o ../src/system_config/default/framework/system/devcon/src/sys_devcon.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c  .generated_files/flags/default/a1f55d2e033c390289bcd9947caa3fed4168d06c .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d" -o ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/822048611/sys_ports_static.o: ../src/system_config/default/framework/system/ports/src/sys_ports_static.c  .generated_files/flags/default/cc52b4432cb748cb024d390718e57e1f457305e5 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/822048611" 
	@${RM} ${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/822048611/sys_ports_static.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d" -o ${OBJECTDIR}/_ext/822048611/sys_ports_static.o ../src/system_config/default/framework/system/ports/src/sys_ports_static.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/68765530/sys_reset.o: ../src/system_config/default/framework/system/reset/src/sys_reset.c  .generated_files/flags/default/fc0845199d2468d8f48a89b4f0d0dfe57a998167 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/68765530" 
	@${RM} ${OBJECTDIR}/_ext/68765530/sys_reset.o.d 
	@${RM} ${OBJECTDIR}/_ext/68765530/sys_reset.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/68765530/sys_reset.o.d" -o ${OBJECTDIR}/_ext/68765530/sys_reset.o ../src/system_config/default/framework/system/reset/src/sys_reset.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1688732426/system_init.o: ../src/system_config/default/system_init.c  .generated_files/flags/default/b235be95ee42113d1ebb3c535e0e5dea5bf7d91b .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_init.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_init.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_init.o ../src/system_config/default/system_init.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1688732426/system_interrupt.o: ../src/system_config/default/system_interrupt.c  .generated_files/flags/default/7de8e5f762157b7858b47681f327c91ca618ec80 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_interrupt.o ../src/system_config/default/system_interrupt.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1688732426/system_exceptions.o: ../src/system_config/default/system_exceptions.c  .generated_files/flags/default/d7d859c440cad59426bf3008225ac906c4cf721b .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_exceptions.o ../src/system_config/default/system_exceptions.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1688732426/system_tasks.o: ../src/system_config/default/system_tasks.c  .generated_files/flags/default/1a7857db3e54f58eb22dcafa021a949dfda2fa05 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_tasks.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_tasks.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_tasks.o ../src/system_config/default/system_tasks.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
else
${OBJECTDIR}/_ext/1360937237/app.o: ../src/app.c  .generated_files/flags/default/5013971924b534ea47d7ce7a86d6985018567671 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/app.o.d" -o ${OBJECTDIR}/_ext/1360937237/app.o ../src/app.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  .generated_files/flags/default/27fce75aa9e40d16382e5028352cf55e712c87ae .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/sounds_allocation.o: ../src/sounds_allocation.c  .generated_files/flags/default/4962f557569cc7ef5d69f0e78dd7c220b5119bf .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/sounds_allocation.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/sounds_allocation.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/sounds_allocation.o.d" -o ${OBJECTDIR}/_ext/1360937237/sounds_allocation.o ../src/sounds_allocation.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/memory.o: ../src/memory.c  .generated_files/flags/default/89f1135215f59ca638a482b3f7aaf07f3b673998 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/memory.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/memory.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/memory.o.d" -o ${OBJECTDIR}/_ext/1360937237/memory.o ../src/memory.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/audio.o: ../src/audio.c  .generated_files/flags/default/c888f3d30216ee0e01e4b822c4e4ffff4f589f0b .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/audio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/audio.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/audio.o.d" -o ${OBJECTDIR}/_ext/1360937237/audio.o ../src/audio.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/ios.o: ../src/ios.c  .generated_files/flags/default/f33f800b5f70163a8780dc8e5748340162664d34 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ios.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ios.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ios.o.d" -o ${OBJECTDIR}/_ext/1360937237/ios.o ../src/ios.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/delay.o: ../src/delay.c  .generated_files/flags/default/e2085ee87ca461888a8a1fd8df003150f4d4da9d .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/delay.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/delay.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/delay.o.d" -o ${OBJECTDIR}/_ext/1360937237/delay.o ../src/delay.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1360937237/parallel_bus.o: ../src/parallel_bus.c  .generated_files/flags/default/4321a3e070158212dd89865878220013d1b6d05e .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/parallel_bus.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/parallel_bus.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/parallel_bus.o.d" -o ${OBJECTDIR}/_ext/1360937237/parallel_bus.o ../src/parallel_bus.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1450069464/drv_i2s_dma.o: ../../../../../../../../../microchip/harmony/v2_04/framework/driver/i2s/src/dynamic/drv_i2s_dma.c  .generated_files/flags/default/b2fea17f66a1aa8ea4fa4654e84231e410a22efa .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1450069464" 
	@${RM} ${OBJECTDIR}/_ext/1450069464/drv_i2s_dma.o.d 
	@${RM} ${OBJECTDIR}/_ext/1450069464/drv_i2s_dma.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1450069464/drv_i2s_dma.o.d" -o ${OBJECTDIR}/_ext/1450069464/drv_i2s_dma.o ../../../../../../../../../microchip/harmony/v2_04/framework/driver/i2s/src/dynamic/drv_i2s_dma.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1383485383/drv_tmr.o: ../../../../../../../../../microchip/harmony/v2_04/framework/driver/tmr/src/dynamic/drv_tmr.c  .generated_files/flags/default/e71ec605374f8fada584c62bb7d9df2ec92f2e35 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1383485383" 
	@${RM} ${OBJECTDIR}/_ext/1383485383/drv_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/1383485383/drv_tmr.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1383485383/drv_tmr.o.d" -o ${OBJECTDIR}/_ext/1383485383/drv_tmr.o ../../../../../../../../../microchip/harmony/v2_04/framework/driver/tmr/src/dynamic/drv_tmr.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/176760579/sys_dma.o: ../../../../../../../../../microchip/harmony/v2_04/framework/system/dma/src/sys_dma.c  .generated_files/flags/default/5fa5b8bc6093637697dec6f0a6bf0d37ec17ecd .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/176760579" 
	@${RM} ${OBJECTDIR}/_ext/176760579/sys_dma.o.d 
	@${RM} ${OBJECTDIR}/_ext/176760579/sys_dma.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/176760579/sys_dma.o.d" -o ${OBJECTDIR}/_ext/176760579/sys_dma.o ../../../../../../../../../microchip/harmony/v2_04/framework/system/dma/src/sys_dma.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/11966580/sys_int_pic32.o: ../../../../../../../../../microchip/harmony/v2_04/framework/system/int/src/sys_int_pic32.c  .generated_files/flags/default/fd16a1b69e19c359bbf8b12c95644c0044c334e6 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/11966580" 
	@${RM} ${OBJECTDIR}/_ext/11966580/sys_int_pic32.o.d 
	@${RM} ${OBJECTDIR}/_ext/11966580/sys_int_pic32.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/11966580/sys_int_pic32.o.d" -o ${OBJECTDIR}/_ext/11966580/sys_int_pic32.o ../../../../../../../../../microchip/harmony/v2_04/framework/system/int/src/sys_int_pic32.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1154096286/sys_tmr.o: ../../../../../../../../../microchip/harmony/v2_04/framework/system/tmr/src/sys_tmr.c  .generated_files/flags/default/1b89530f634525bee133149af312c3a9b04a10e9 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1154096286" 
	@${RM} ${OBJECTDIR}/_ext/1154096286/sys_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/1154096286/sys_tmr.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1154096286/sys_tmr.o.d" -o ${OBJECTDIR}/_ext/1154096286/sys_tmr.o ../../../../../../../../../microchip/harmony/v2_04/framework/system/tmr/src/sys_tmr.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/134701902/drv_usbhs.o: ../../../../../../../../../microchip/harmony/v2_04/framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c  .generated_files/flags/default/666b785aad64938769605fdf8aac4e2c90dfdf4f .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/134701902" 
	@${RM} ${OBJECTDIR}/_ext/134701902/drv_usbhs.o.d 
	@${RM} ${OBJECTDIR}/_ext/134701902/drv_usbhs.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/134701902/drv_usbhs.o.d" -o ${OBJECTDIR}/_ext/134701902/drv_usbhs.o ../../../../../../../../../microchip/harmony/v2_04/framework/driver/usb/usbhs/src/dynamic/drv_usbhs.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/249061497/usb_device.o: ../../../../../../../../../microchip/harmony/v2_04/framework/usb/src/dynamic/usb_device.c  .generated_files/flags/default/7527876b88168849179e1431fa72fe8d41e310c7 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/249061497" 
	@${RM} ${OBJECTDIR}/_ext/249061497/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/249061497/usb_device.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/249061497/usb_device.o.d" -o ${OBJECTDIR}/_ext/249061497/usb_device.o ../../../../../../../../../microchip/harmony/v2_04/framework/usb/src/dynamic/usb_device.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/134701902/drv_usbhs_device.o: ../../../../../../../../../microchip/harmony/v2_04/framework/driver/usb/usbhs/src/dynamic/drv_usbhs_device.c  .generated_files/flags/default/c541f9d316aedd6d70f5d334b98c0a10c62e149b .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/134701902" 
	@${RM} ${OBJECTDIR}/_ext/134701902/drv_usbhs_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/134701902/drv_usbhs_device.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/134701902/drv_usbhs_device.o.d" -o ${OBJECTDIR}/_ext/134701902/drv_usbhs_device.o ../../../../../../../../../microchip/harmony/v2_04/framework/driver/usb/usbhs/src/dynamic/drv_usbhs_device.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/249061497/usb_device_endpoint_functions.o: ../../../../../../../../../microchip/harmony/v2_04/framework/usb/src/dynamic/usb_device_endpoint_functions.c  .generated_files/flags/default/8e2709318d0bc7583a8ee3266de2fddb0e6bf5fe .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/249061497" 
	@${RM} ${OBJECTDIR}/_ext/249061497/usb_device_endpoint_functions.o.d 
	@${RM} ${OBJECTDIR}/_ext/249061497/usb_device_endpoint_functions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/249061497/usb_device_endpoint_functions.o.d" -o ${OBJECTDIR}/_ext/249061497/usb_device_endpoint_functions.o ../../../../../../../../../microchip/harmony/v2_04/framework/usb/src/dynamic/usb_device_endpoint_functions.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o: ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c  .generated_files/flags/default/a7da7ccef8ec3fe41452d1386b325fe4b1eb7916 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/639803181" 
	@${RM} ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d" -o ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/340578644/sys_devcon.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon.c  .generated_files/flags/default/6d22082de510f2b21b2a9e5bfa70d9d755bdb99f .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon.o.d" -o ${OBJECTDIR}/_ext/340578644/sys_devcon.o ../src/system_config/default/framework/system/devcon/src/sys_devcon.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c  .generated_files/flags/default/2388ba72ec5ad2c7a9e5021fd94aa987896dd829 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d" -o ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/822048611/sys_ports_static.o: ../src/system_config/default/framework/system/ports/src/sys_ports_static.c  .generated_files/flags/default/2baf96286652c5dc790a85f1fd89677ffbbe628d .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/822048611" 
	@${RM} ${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/822048611/sys_ports_static.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d" -o ${OBJECTDIR}/_ext/822048611/sys_ports_static.o ../src/system_config/default/framework/system/ports/src/sys_ports_static.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/68765530/sys_reset.o: ../src/system_config/default/framework/system/reset/src/sys_reset.c  .generated_files/flags/default/1dc3b284a6e572cf895fc1dcf4b2bd17bffc98a .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/68765530" 
	@${RM} ${OBJECTDIR}/_ext/68765530/sys_reset.o.d 
	@${RM} ${OBJECTDIR}/_ext/68765530/sys_reset.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/68765530/sys_reset.o.d" -o ${OBJECTDIR}/_ext/68765530/sys_reset.o ../src/system_config/default/framework/system/reset/src/sys_reset.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1688732426/system_init.o: ../src/system_config/default/system_init.c  .generated_files/flags/default/b0c9b1dcc739124854c760549ab5029e06133c73 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_init.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_init.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_init.o ../src/system_config/default/system_init.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1688732426/system_interrupt.o: ../src/system_config/default/system_interrupt.c  .generated_files/flags/default/677963f2ca5b1e8b6950d72de114646e561e9910 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_interrupt.o ../src/system_config/default/system_interrupt.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1688732426/system_exceptions.o: ../src/system_config/default/system_exceptions.c  .generated_files/flags/default/76d41842b07e7fe481d50e18609022fe59956f81 .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_exceptions.o ../src/system_config/default/system_exceptions.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
${OBJECTDIR}/_ext/1688732426/system_tasks.o: ../src/system_config/default/system_tasks.c  .generated_files/flags/default/d5b0a643c667d72735f42a262a25ecd830f7a7fa .generated_files/flags/default/b9a611e7d3f9e13f36a974aabff414fb1e968c
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_tasks.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O3 -fno-common -I"../../../microchip/harmony/v2_04/framework" -I"../../microchip/harmony/v2_04/framework" -I"../../../../../../../microchip/harmony/v2_04/framework" -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../../../../../../microchip/harmony/v2_04/framework" -I"../src/system_config/default/framework" -MP -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_tasks.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_tasks.o ../src/system_config/default/system_tasks.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/SoundCard.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../../microchip/harmony/v2_04/bin/framework/peripheral/PIC32MZ2048EFM100_peripherals.a ../../../../../../../../../microchip/harmony/v2_04/bin/framework/peripheral/PIC32MZ2048EFM100_peripherals.a  ../src/system_config/default/app_mz.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g -mdebugger -D__MPLAB_DEBUGGER_ICD4=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/SoundCard.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ..\..\microchip\harmony\v2_04\bin\framework\peripheral\PIC32MZ2048EFM100_peripherals.a ..\..\..\..\..\..\..\..\..\microchip\harmony\v2_04\bin\framework\peripheral\PIC32MZ2048EFM100_peripherals.a      -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   -mreserve=data@0x0:0x37F   -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=__MPLAB_DEBUGGER_ICD4=1,--defsym=_min_heap_size=0,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/SoundCard.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../../microchip/harmony/v2_04/bin/framework/peripheral/PIC32MZ2048EFM100_peripherals.a ../../../../../../../../../microchip/harmony/v2_04/bin/framework/peripheral/PIC32MZ2048EFM100_peripherals.a ../src/system_config/default/app_mz.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/SoundCard.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ..\..\microchip\harmony\v2_04\bin\framework\peripheral\PIC32MZ2048EFM100_peripherals.a ..\..\..\..\..\..\..\..\..\microchip\harmony\v2_04\bin\framework\peripheral\PIC32MZ2048EFM100_peripherals.a      -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=0,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml 
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/SoundCard.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
